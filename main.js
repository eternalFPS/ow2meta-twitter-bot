var fs = require("fs");
var https = require("https");
const {TwitterApi} = require("twitter-api-v2");
const cfg = require("./cfg.json");

/* HOW THE BOT WORKS:
 *  1. We download overbuff page.
 *  2. From there, we look for the string "__NEXT_DATA__", and track the location of that.
 *  3. From that location, we look for a json object. That json object has all the pickrates
 *  4. Then we go through and find top 3 of each role
 *  5. Then we open up the file "lastmeta" and see if the meta has changed
 *  6. If it hasn't changed, then we stop the program
 *  7. It it HAS changed, we then write the new meta to the same file "lastmeta"
 *  8. We then produce a tweet message
 *  9. And then send off the tweet
 *
 *  FOR FUTURE USE:
 *    - Will need to add new Heros manually. This sucks but even if you did automate it, you will still need to do emojis for the characters. Should be easy to fix whenever it happens.
 */

const client = new TwitterApi({
  appKey: cfg.consumer_key,
  appSecret: cfg.consumer_secret,
  accessToken: cfg.access_token,
  accessSecret: cfg.access_secret
});

c = client.readWrite;


var options = {
  headers: {
    'User-Agent': 'ow2meta Twitter bot'//,
//    "Accept-Encoding": "gzip"
  }
};

url = "https://www.overbuff.com/heroes?platform=pc&gameMode=competitive&skillTier=grandmaster&timeWindow=month"

https.get(url, options, function(res) {
  console.log("Got response: " + res.statusCode);
  console.log(res.headers);
  res.setEncoding('utf8');
  fullHtml = "";
  res.on("data", function(data){
    fullHtml+=data;
    console.log("Downloading: "+fullHtml.length);
  });
  res.on("end", function(){
    console.log("finish");
    cata = fullHtml.split('');
    ind = endSubStr(cata, "__NEXT_DATA__");
    jay = findJson(cata, ind);
    hData = jay.props.pageProps.aggregates;
            //0:dps       1:tank        2:support
    top3d = [new Array(3), new Array(3), new Array(3)]; //id
    top3i = [0,0,0];  //current index we are working with
    for(let i=0;i<hData.length;i++){
      id = hData[i].heroId;
      rol = -1;
      switch(heroRole(id)){
        case 1: rol = 0; break;
        case 3: rol = 1; break;
        case 4: rol = 2; break;
        default: continue;
      }
      if(top3i[rol]>2) continue;
      top3d[rol][top3i[rol]++] = id;
      if(top3i[0] > 2 && top3i[1] > 2 && top3i[2]>2) break;
    }
    console.log("NEW:");
    console.log(top3d);
    fs.readFile("lastmeta","utf8",function(err,data){
      if(err) throw new Error(err);
      lns = data.split('\n');
      oldM = [
        lns[0].split(','),
        lns[1].split(','),
        lns[2].split(',')
      ];
      console.log("OLD:");
      console.log(oldM);
      outStr="";
      changed=false;
      for(let i=0;i<9;i++){
        x = i%3;
        y= Math.floor(i/3);
        if(parseInt(top3d[y][x])!=parseInt(oldM[y][x])) changed=true;
        outStr+=top3d[y][x];
        if(x==2) outStr+='\n';
        else outStr+=',';
      }
      if(!changed) {
        console.log("Metas are the same. Quitting execution");
        return;
      }
      //write to "db"
      // SKETCHY, because we are still READING the file lol.
      fs.writeFile('lastmeta', outStr, err => {
        if (err) {
          console.error(err);
        }
      });
      //format and send message
      twStr = "Meta Update ("+shortDate()+")\n";
      roles = ["DPS","TANK","SUPPORT"];
      for(let i=0;i<9;i++){
        x = i%3;
        y= Math.floor(i/3);
        if(x==0) twStr+='\n'+roles[y]+'\n';
        twStr+=heroName(top3d[y][x])+'\n';
      }
      console.log(twStr);
      c.v2.tweet(twStr).then(function(data){
        console.log(data);
      }).catch(function(err){
        console.log(err);
      });
    });
  });
}).on('error', function(e) {
  console.log("Got error: " + e.message);
});

function heroName(id){
  switch(id){
    case 0:
       return "All";
     case 1022:
       return "👵Ana";
     case 1029:
       return "👢Ashe";
     case 1030:
       return "🪟Baptiste";
     case 1001:
       return "🤖Bastion";
     case 1027:
       return "🚫Brigitte";
     case 1007:
       return "🤠McCree";
     case 1025:
       return "👊🏿Doomfist";
     case 1002:
       return "🎮D.Va";
     case 1032:
       return "🪞Echo";
     case 1003:
       return "🥷Genji";
     case 1004:
       return "🏹Hanzo";
     case 1005:
       return "🐀Junkrat";
     case 1033:
       return "♕Junker Queen";
     case 1034:
       return "🎟️Kiriko";
     case 1037:
       return "💮Lifeweaver";
     case 1006:
       return "🔊Lucio";
     case 1008:
       return "🥶Mei";
     case 1009:
       return "😇Mercy";
     case 1026:
       return "🤮Moira";
     case 1024:
       return "🐎Orisa";
     case 1010:
       return "🪽Pharah";
     case 1036:
       return "🟣Ramattra";
     case 1011:
       return "💀Reaper";
     case 1012:
       return "🛡️Reinhardt";
     case 1013:
       return "🐷Roadhog";
     case 1031:
       return "ΣSigma";
     case 1035:
       return "🛝Sojourn";
     case 1014:
       return "🪖Soldier: 76";
     case 1023:
       return "🖥️Sombra";
     case 1015:
       return "♋Symmetra";
     case 1016:
       return "🔧Torbjorn";
     case 1017:
       return "🗲Tracer";
     case 1018:
       return "🕷️Widowmaker";
     case 1019:
       return "🦍Winton";
     case 1028:
       return "🐹Wrecking Ball";
     case 1020:
       return "⚡Zarya";
     case 1021:
       return "☮️Zenyatta";
      default:
        return id;
  }
}

function heroRole(id){
  switch(id){
     case 0: 
       return 0;
     case 1022: 
       return 4;
     case 1029: 
       return 1;
     case 1030: 
       return 4;
     case 1001: 
       return 1;
     case 1027: 
       return 4;
     case 1007: 
       return 1;
     case 1025: 
       return 3;
     case 1002: 
       return 3;
     case 1032: 
       return 1;
     case 1003: 
       return 1;
     case 1004: 
       return 1;
     case 1005: 
       return 1;
     case 1033: 
       return 3;
     case 1034: 
       return 4;
     case 1037: 
       return 4;
     case 1006: 
       return 4;
     case 1008: 
       return 1;
     case 1009: 
       return 4;
     case 1026: 
       return 4;
     case 1024: 
       return 3;
     case 1010: 
       return 1;
     case 1036: 
       return 3;
     case 1011: 
       return 1;
     case 1012: 
       return 3;
     case 1013: 
       return 3;
     case 1031: 
       return 3;
     case 1035: 
       return 1;
     case 1014: 
       return 1;
     case 1023: 
       return 1;
     case 1015: 
       return 1;
     case 1016: 
       return 1;
     case 1017: 
       return 1;
     case 1018: 
       return 1;
     case 1019: 
       return 3;
     case 1028: 
       return 3;
     case 1020: 
       return 3;
     case 1021: 
       return 4;
    default:
      return 0;
  }
}

function shortDate(){
  months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
  nao = new Date();
  outDate = "";
  dayNo = nao.getDate();
  if(dayNo<10) outDate+='0'+dayNo;
  else outDate+=dayNo;
  outDate+=months[nao.getMonth()];
  outDate+=(""+nao.getFullYear()).slice(-2);
  return outDate;
}

/*returns the location of the final character of a substring within a string
  e.g.
  0123456789
  I was walk

  endSubStr(str, "was");
  returns 4

  returns -1 if not found

*/
function endSubStr(str, ser){
  //cStr = str.split('');
  cStr = str; //for efficiencies sake, we have already converted
  cSer = ser.split('');
  count=0;
  index=0;
  while(index<str.length){
    if(cStr[index]==cSer[count]){
      count++;
    }else count=0;
    if(count>=ser.length) return index;
    index++;
  }
  return -1;
}

/* returns a json object hidden within a string
 * "" if it can't find shit
 */
function findJson(str,index=0){
  //cStr = str.split('');
  cStr = str;
  start = -1;
  depth = 0;
  while(index<str.length){
    if(cStr[index]=='{'){
      if(start<0) start=index;
      depth++;
    }else if(cStr[index]=='}') depth--;
    if(start>-1&&depth==0) {
      //return JSON.parse(str.slice(start, index+1));
      return JSON.parse(str.slice(start, index+1).join(''));
    }
    index++;
  }
  return "";
}
